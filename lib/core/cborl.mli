(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** OCaml implementation of The Concise Binary Object Representation (CBOR)

This module provides types for CBOR data items as well as function for
writing CBOR data items to sequences of bytes and reading CBOR data
items from sequences of bytes.

@see <https://www.rfc-editor.org/rfc/rfc8949.html> RFC 8949 Concise Binary Object Representation (CBOR)
*)

(** {1 CBOR Data Item} *)

(** Type of CBOR data item *)
type item =
  | Null
  | Undefined
  | Bool of bool
  | Integer of Z.t
  | ByteString of string
  | TextString of string
  | Array of item list
  | Map of (item * item) list
  | Tag of Z.t * item
  | Simple of int
  | Float of float
  | Double of float

val pp : item Fmt.t
  [@@ocaml.toplevel_printer]
(** CBOR data item pretty printer. Output resembles the CBOR diagnostic notation. *)

val equal : item -> item -> bool
(** [equal a b] returns [true] if [a] is structurally equal to [b]. *)

(** {1 Serialization} *)

val read : char Seq.t -> item
(** [read bytes] read a single CBOR data item from the sequence [bytes].

To read multiple data items (i.e. a CBOR data stream) see {!Signal.to_items}. *)

val write : item -> char Seq.t
(** [write item] returns a sequence of bytes that encodes [item]. *)

(** {2 Signals} *)

module Signal : sig
  (** Serialization is done by converting CBOR data items to a
  sequence of signals. Signals can be directly translated to and from
  bytes. This module provides direct access to signals. This can be used to
  stream large CBOR data items and also provides the ability to work
  with indefinite-length CBOR data items. *)

  (** Type of CBOR Signal *)
  type t =
    | Null
    | Undefined
    | Bool of bool
    | Integer of Z.t  (** The integer value must fit into 8 bytes. *)
    | ByteString of int
        (** In a well-formed sequence of signals, a [ByteString n] element is always followed by exactly [n] [Byte b] elements. *)
    | TextString of int
        (** In a well-formed sequence of signals, a [TextString n] element is always followed by exactly [n] [Byte b] elements. *)
    | Byte of char
    | Array of int option
        (** In a well-formed sequence of signals, an [Array n_opt] element is followed by exaclty [n] well-formed sequence of CBOR items when [n_opt = Some n] or by an indefinite number of items followed by a [Break] element. *)
    | Break
    | Map of int option
        (** In a well-formed sequence of signals, an [Map n_opt] element is followed by exaclty [n] well-formed sequence of CBOR items when [n_opt = Some n] or by an indefinite number of items followed by a [Break] element. *)
    | Tag of Z.t
        (** In a well-formed sequence of signals, a [Tag t] element is followed by a single sequence of an encoded CBOR item. *)
    | Simple of int
    | Float of float
    | Double of float

  val pp : t Fmt.t

  exception NotWellFormed of string
  (** Exception that is thrown when sequence of signals or encoding is not well formed. *)

  (** {1 Conversion from and to CBOR Data Items} *)

  val of_item : item -> t Seq.t
  (** [of_item item] returns a sequence of signals that represents the CBOR data item [item].

Note that [Cborl.Integer n] values are encoded as Bignums if [n] is larger than what can be encoded in 8 bytes.*)

  val to_items : t Seq.t -> item Seq.t
  (** [to_items signals] reads multiple CBOR data items from the sequence [signals]. *)

  val to_item : t Seq.t -> item * t Seq.t
  (** [to_item signals] reads one item from the sequence [signals] and returns the sequence containing the continuation of signals after the read item. *)

  (** {2 Indefinite-Length Data Items} *)

  val indefinite_length_array : item Seq.t -> t Seq.t
  (** [indefinite_length_array items] returns a sequence of signals encoding [items] as an indefinite-length array. *)

  val indefinite_length_map : (item * item) Seq.t -> t Seq.t
  (** [indefinite_length_array key_values] returns a sequence of signals encoding [key_values] as an indefinite-length map. *)

  (** {1 Writing} *)

  val write : t Seq.t -> char Seq.t
  (** [write stream] *)

  (** {1 Reading } *)

  val read : char Seq.t -> t Seq.t
  (** [read input] *)
end
