(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let z_gen =
  QCheck.Gen.(
    pair bool ui64 >|= fun (negative, uint64) ->
    if negative then Z.(neg @@ of_int64 uint64) else Z.(of_int64 uint64))

let bignum_gen =
  QCheck.Gen.(
    pair bool (string_size small_nat) >|= fun (negative, bytes) ->
    if negative then Z.(neg @@ of_bits bytes) else Z.(of_bits bytes))

let item_gen max_size =
  ignore z_gen;
  QCheck.Gen.(
    sized_size (int_bound max_size)
    @@ fix (fun self n ->
           frequency
             [
               (20, z_gen >|= fun z -> Cborl.Integer z);
               (20, bignum_gen >|= fun z -> Cborl.Integer z);
               (* also generate some small ints *)
               (20, small_int >|= fun i -> Cborl.Integer (Z.of_int i));
               (20, return Cborl.Null);
               (20, return Cborl.Undefined);
               (20, bool >|= fun b -> Cborl.Bool b);
               (20, string >|= fun s -> Cborl.ByteString s);
               (20, string_printable >|= fun s -> Cborl.TextString s);
               (* Generate floats by generating Int32. This prevents
                  lossfull casting from double *)
               (20, ui32 >|= fun i32 -> Cborl.Float (Int32.float_of_bits i32));
               (20, ui64 >|= fun i64 -> Cborl.Double (Int64.float_of_bits i64));
               ( 1,
                 pair z_gen (self (n / 2)) >|= fun (tag, content) ->
                 Cborl.Tag (Z.abs tag, content) );
               (20, int_range 0 19 >|= fun n -> Cborl.Simple n);
               (20, int_range 32 255 >|= fun n -> Cborl.Simple n);
               ( 1,
                 list_size (int_bound n) (pair (self 1) (self (n / 2)))
                 >|= fun m -> Cborl.Map m );
               ( 1,
                 list_size (int_bound n) (self (n / 2)) >|= fun l ->
                 Cborl.Array l );
             ]))

let cbor ~max_size =
  let rec shrink =
    QCheck.Shrink.(
      function
      | Cborl.Array a -> QCheck.Iter.(list a >|= fun a -> Cborl.Array a)
      | Cborl.Map a -> QCheck.Iter.(list a >|= fun a -> Cborl.Map a)
      | Cborl.Tag (t, c) -> QCheck.Iter.(shrink c >|= fun c -> Cborl.Tag (t, c))
      | i -> QCheck.Iter.return i)
  in
  QCheck.make
    ~print:(fun item -> Format.asprintf "%a" Cborl.pp item)
    ~shrink (item_gen max_size)

let check_equal a b =
  if Cborl.equal a b then true
  else (
    Format.eprintf "CBOR not equal. Expecting @[<4>%a@] but got @[<4>%a@].\n"
      Cborl.pp a Cborl.pp b;
    false)

let item_testable = Alcotest.testable Cborl.pp Cborl.equal

let test_read_write =
  QCheck.Test.make ~count:1000 ~name:"Write and read random items."
    (cbor ~max_size:1000) (fun item ->
      let decoded = item |> Cborl.write |> Cborl.read in
      (* Alcotest.check item_testable "decodes is equal to encoded item" item *)
      (* decoded; *)
      check_equal decoded item)

let test_large_byte_string =
  QCheck.Test.make ~count:2 ~name:"Encode large byte string."
    QCheck.(string_of_size (Gen.return 0xffff))
    (fun s ->
      let item = Cborl.ByteString s in
      let encoded = Cborl.write item |> String.of_seq in
      let decoded = encoded |> String.to_seq |> Cborl.read in
      check_equal decoded item)

let test_deeply_nested_array =
  let nested_array n =
    let rec build n acc =
      match n with
      | 0 -> acc (Cborl.Array [])
      | n -> build (n - 1) (fun inner -> acc @@ Cborl.Array [ inner ])
    in
    build n (fun x -> x)
  in

  Alcotest.test_case "Encode deeply nested array." `Quick (fun () ->
      let item = nested_array 10000 in
      let encoded = Cborl.write item |> String.of_seq in
      let decoded = encoded |> String.to_seq |> Cborl.read in
      Alcotest.check item_testable "decoded item is equal to item" decoded item)

let test_indefinite_array =
  QCheck.Test.make ~count:10 ~name:"Encode indefinite-length array."
    (QCheck.list @@ cbor ~max_size:10)
    (fun items ->
      let signals = Cborl.Signal.indefinite_length_array (List.to_seq items) in
      let encoded = Cborl.Signal.write signals |> String.of_seq in
      let decoded = encoded |> String.to_seq |> Cborl.read in

      check_equal (Cborl.Array items) decoded)

let test_indefinite_map =
  QCheck.Test.make ~count:10 ~name:"Encode indefinite-length map."
    QCheck.(list @@ pair (cbor ~max_size:10) (cbor ~max_size:10))
    (fun items ->
      let signals = Cborl.Signal.indefinite_length_map (List.to_seq items) in
      let encoded = Cborl.Signal.write signals |> String.of_seq in
      let decoded = encoded |> String.to_seq |> Cborl.read in

      (* decoded; *)
      check_equal (Cborl.Map items) decoded)

module Examples = struct
  (** Examples taken from RFC 8949
  (https://www.rfc-editor.org/rfc/rfc8949.html#name-examples-of-encoded-cbor-da) *)

  let examples =
    (* TODO: add all examples *)
    Z.(
      Cborl.
        [
          (Integer ~$0, [ 0x00 ]);
          (Integer ~$1, [ 0x01 ]);
          (Integer ~$10, [ 0x0a ]);
          (Integer ~$23, [ 0x17 ]);
          (Integer ~$24, [ 0x18; 0x18 ]);
          ( Integer (of_string "1000000000000"),
            [ 0x1b; 0x00; 0x00; 0x00; 0xe8; 0xd4; 0xa5; 0x10; 0x00 ] );
          ( Integer (of_string "18446744073709551615"),
            [ 0x1b; 0xff; 0xff; 0xff; 0xff; 0xff; 0xff; 0xff; 0xff ] );
          ( Integer (of_string "18446744073709551616"),
            [ 0xc2; 0x49; 0x01; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00 ]
          );
          ( Integer (of_string "-18446744073709551616"),
            [ 0x3b; 0xff; 0xff; 0xff; 0xff; 0xff; 0xff; 0xff; 0xff ] );
          ( Integer (of_string "-18446744073709551617"),
            [ 0xc3; 0x49; 0x01; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00 ]
          );
          (Integer ~$(-1), [ 0x20 ]);
          (Double 1.1, [ 0xfb; 0x3f; 0xf1; 0x99; 0x99; 0x99; 0x99; 0x99; 0x9a ]);
          (Float 100000.0, [ 0xfa; 0x47; 0xc3; 0x50; 0x00 ]);
          (Simple 16, [ 0xf0 ]);
          (Simple 255, [ 0xf8; 0xff ]);
          ( Tag (~$1, Integer ~$1363896240),
            [ 0xc1; 0x1a; 0x51; 0x4b; 0x67; 0xb0 ] );
          ( Tag (~$32, TextString "http://www.example.com"),
            [
              0xd8;
              0x20;
              0x76;
              0x68;
              0x74;
              0x74;
              0x70;
              0x3a;
              0x2f;
              0x2f;
              0x77;
              0x77;
              0x77;
              0x2e;
              0x65;
              0x78;
              0x61;
              0x6d;
              0x70;
              0x6c;
              0x65;
              0x2e;
              0x63;
              0x6f;
              0x6d;
            ] );
        ])

  let binary_testable =
    Alcotest.testable Fmt.(on_string @@ octets ()) String.equal

  let to_test (cbor, expected_encoding) =
    Alcotest.test_case (Format.asprintf "%a" Cborl.pp cbor) `Quick (fun () ->
        let expected_encoding =
          expected_encoding |> List.to_seq |> Seq.map Char.chr |> String.of_seq
        in

        let encoded = Cborl.write cbor |> String.of_seq in

        Alcotest.check binary_testable "" expected_encoding encoded)

  let test_cases = List.map to_test examples
end

let () =
  Alcotest.run "Cborl"
    [
      ( "Cborl",
        List.map QCheck_alcotest.to_alcotest
          [ test_read_write; test_large_byte_string ]
        @ [ test_deeply_nested_array ] );
      ("RFC 8949 Examples", Examples.test_cases);
      ( "Cborl.Signal",
        List.map QCheck_alcotest.to_alcotest
          [ test_indefinite_array; test_indefinite_map ] );
    ]
