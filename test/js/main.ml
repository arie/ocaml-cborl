let file_in = Config_local.file_in
let file_out = Config_local.file_out

let seq_of_file file =
  let ic = open_in file in
  let seq = Seq.of_dispenser (fun () -> In_channel.input_char ic) in
  seq

let seq_to_file file_out pp seq =
  let oc = open_out file_out in
  let formatter = Format.formatter_of_out_channel oc in
  seq
  |> Seq.iter (fun x ->
      Fmt.pf formatter "%a" pp x;
      flush_all ()
    );
  close_out oc

(* Some simple functions to test when stackoverflows or
 * heap memory overflows might happen *)
let _helper k head seq =
  Seq.cons head (k seq)

let rec _recursive_id seq () = match Seq.uncons seq with
  | Some (head, tail) -> _helper _recursive_id head tail ()
  | None -> Seq.Nil

let _id x = x

(* Basic read and write function. When f = _id, it is
 * the simplest example you can make
 * -- with flush: works
 * -- without: doesn't work *)
let _read_function_write f =
  file_in
  |> seq_of_file
  |> f
  |> seq_to_file file_out Fmt.char

(* -- with flush: works
 * -- without: doesn't work *)
let _read_cbor_out () =
  file_in
  |> seq_of_file
  |> Cborl.Signal.read
  |> Cborl.Signal.write
  |> seq_to_file file_out Fmt.char

(* -- with flush: works
 * -- without: doesn't work *)
let _read_cbor_pp_signal_out () =
  file_in
  |> seq_of_file
  |> Cborl.Signal.read
  |> seq_to_file file_out Cborl.Signal.pp

(* -- with flush: works
 * -- without: doesn't work *)
let _read_cbor_pp_to_string_out () =
  file_in
  |> seq_of_file
  |> Cborl.Signal.read
  |> Seq.map (fun s -> Fmt.str "%a" Cborl.Signal.pp s)
  |> seq_to_file file_out Fmt.string

(* Simple sequence written to a file.
 * -- with flush: works
 * -- without: doesn't work *)
let _test_iter () =
  let ppf =
    let oc = open_out file_out in
    Format.formatter_of_out_channel oc in
  Seq.forever (fun () -> 'a')
  |> Seq.take 10000000
  |> Seq.iter (fun c -> Fmt.pf ppf "%c" c; flush_all ())

(* let () = _read_function_write _id *)
(* let () = _read_cbor_pp_signal_out () *)
(* let () = _read_cbor_pp_to_string_out () *)
(* let () = _read_cbor_out () *)
let () = _test_iter ()
