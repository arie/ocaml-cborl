(use-modules
 (guix packages)
 (guix build-system dune)
 ((guix licenses) #:prefix license:)
 (gnu packages ocaml))

(define-public ocaml-cborl
  (package
    (name "ocaml-cborl")
    (version "1.0.0")
    (source #f)
    (build-system dune-build-system)
    (arguments '())
    (propagated-inputs
     (list ocaml-zarith ocaml-fmt))
    (native-inputs
     (list ocaml-alcotest ocaml-qcheck))
    (home-page "https://inqlab.net/git/ocaml-cborl")
    (synopsis "OCaml CBOR library")
    (description 
     "The Concise Binary Object Representation (CBOR), as specified by
RFC 8949, is a binary data serialization format.  CBOR is similar to
JSON but serializes to binary which is smaller and faster to generate
and parse.  This package provides an OCaml implementation of CBOR.")
    (license license:isc)))

ocaml-cborl
